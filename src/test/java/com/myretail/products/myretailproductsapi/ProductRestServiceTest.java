package com.myretail.products.myretailproductsapi;

import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.service.ProductRestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 6/18/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyRetailProductsApiApplication.class,
        properties = {
                "spring.profiles.active=dev,unit-test"
        })
public class ProductRestServiceTest {
    @Autowired
    private ProductRestService productRestService;

    @Test
    public void contextLoads() {
        assertThat(productRestService).isNotNull();
    }

    @Test
    public void findProductById() throws IOException {
        Product product = productRestService.findProduct(13860428L);

        assertThat(product).isNotNull();
        assertThat(product.getId()).isNotNull().isGreaterThan(0).isEqualTo(13860428L);
    }

    @Test
    public void productDoesNotExistOnEndpoint() throws IOException {
        Product product = productRestService.findProduct(15117729L);
        assertThat(product).isNull();
    }
}
