package com.myretail.products.myretailproductsapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyRetailProductsApiApplication.class,
		properties = {
				"spring.profiles.active=dev,unit-test"
		})
public class MyRetailProductsApiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
