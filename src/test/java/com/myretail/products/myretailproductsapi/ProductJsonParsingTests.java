package com.myretail.products.myretailproductsapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.rest.ProductItem;
import com.myretail.products.myretailproductsapi.rest.ProductResponse;
import com.myretail.products.myretailproductsapi.util.file.FileUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 6/17/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyRetailProductsApiApplication.class,
        properties = {
                "spring.profiles.active=dev,unit-test"
        })
public class ProductJsonParsingTests {
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void contextLoads() {
        assertThat(objectMapper).isNotNull();
    }

    @Test
    public void readAndConvertJsonFile() throws IOException {
        List<String> fileContent = FileUtil.getResource("data/13860428.json");
        assertThat(fileContent).isNotNull();
        assertThat(fileContent.size()).isGreaterThan(0);

        String jsonContent = fileContent.stream().collect(Collectors.joining());
        ProductResponse productResponse = objectMapper.readValue(jsonContent, ProductResponse.class);

        assertThat(productResponse).isNotNull();
        assertThat(productResponse.getProductJson()).isNotNull();
        assertThat(productResponse.getProductJson().getAvailableToPromise()).isNotNull();
        assertThat(productResponse.getProductJson().getAvailableToPromise().getProductId())
                .isNotNull().isGreaterThan(0L).isEqualTo(13860428L);

        assertThat(productResponse.getProductJson().getProductItem()).isNotNull();
        assertThat(productResponse.getProductJson().getProductItem().getTcin())
                .isNotNull().isEqualTo("13860428");
        assertThat(productResponse.getProductJson().getProductItem().getProductDescription()).isNotNull();
        assertThat(productResponse
                .getProductJson()
                .getProductItem()
                .getProductDescription()
                .getTitle())
                .isNotNull()
                .isEqualTo("The Big Lebowski (Blu-ray)");
    }

    @Test
    public void parseProductFromResponse() throws IOException {
        List<String> fileContent = FileUtil.getResource("data/13860428.json");
        assertThat(fileContent).isNotNull();
        assertThat(fileContent.size()).isGreaterThan(0);

        String jsonContent = fileContent.stream().collect(Collectors.joining());
        ProductResponse productResponse = objectMapper.readValue(jsonContent, ProductResponse.class);

        assertThat(productResponse).isNotNull();
        assertThat(productResponse.getProductJson()).isNotNull();
        assertThat(productResponse.getProductJson().getProductItem()).isNotNull();

        Product product = ProductItem.map(productResponse.getProductJson().getProductItem());

        assertThat(product).isNotNull();
        assertThat(product.getId()).isNotNull().isGreaterThan(0).isEqualTo(13860428L);
        assertThat(product.getName()).isNotNull().isNotEmpty().isEqualTo("The Big Lebowski (Blu-ray)");
    }
}
