package com.myretail.products.myretailproductsapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myretail.products.myretailproductsapi.domain.CurrencyCode;
import com.myretail.products.myretailproductsapi.domain.CurrentPrice;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.repository.ProductRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 6/17/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyRetailProductsApiApplication.class,
        properties = {
                "spring.profiles.active=dev,unit-test"
        })
public class ProductRepositoryTest {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void contextLoads() {
        assertThat(productRepository).isNotNull();
        assertThat(objectMapper).isNotNull();
    }


    @After
    public void removeAllProducts(){
        productRepository.deleteAll();
    }

    @Test
    public void saveSampleProduct() {
        Long productId = 15117729L;
        Product product = Product.builder()
                .name("Beats")
                .id(productId)
                .currentPrice(CurrentPrice.builder()
                        .currencyCode(CurrencyCode.USD)
                        .value(new BigDecimal(199.99))
                        .build())
                .build();

        product = productRepository.save(product);

        assertThat(product).isNotNull();
        assertThat(product.getId()).isNotNull();
    }

    @Test
    public void saveAndFindSampleProduct() throws JsonProcessingException {
        Long productId = 15117729L;
        Product product = Product.builder()
                .name("Beats")
                .id(productId)
                .currentPrice(CurrentPrice.builder()
                        .currencyCode(CurrencyCode.USD)
                        .value(new BigDecimal(199.99))
                        .build())
                .build();

        product = productRepository.save(product);

        assertThat(product).isNotNull();
        assertThat(product.getId()).isNotNull();

        Product foundProduct = productRepository.findOne(productId);
        assertThat(foundProduct).isNotNull();

        Optional<Product> optionalProduct = productRepository.findById(productId);

        assertThat(optionalProduct).isNotNull().isPresent();
        Product secondFound = optionalProduct.orElse(null);
        assertThat(secondFound).isNotNull();

        System.out.println(objectMapper.writeValueAsString(secondFound));
    }

}
