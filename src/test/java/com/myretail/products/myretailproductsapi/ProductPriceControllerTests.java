package com.myretail.products.myretailproductsapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myretail.products.myretailproductsapi.controller.ProductDto;
import com.myretail.products.myretailproductsapi.domain.CurrencyCode;
import com.myretail.products.myretailproductsapi.domain.CurrentPrice;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.repository.ProductRepository;

import com.myretail.products.myretailproductsapi.rest.ProductItem;
import com.myretail.products.myretailproductsapi.rest.ProductResponse;
import com.myretail.products.myretailproductsapi.util.file.FileUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by 6/19/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyRetailProductsApiApplication.class,
        properties = {
                "spring.profiles.active=dev,unit-test"
        })
@WebAppConfiguration
public class ProductPriceControllerTests {
    private static final Logger logger = LoggerFactory.getLogger(ProductPriceControllerTests.class);
    private static long productId = 13860428;
    MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
    //HttpMessageConverter mappingJackson2HttpMessageConverter;
    MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    private WebApplicationContext webApplicationContext;

    /**
     * Cleanup the local database for product prices after each test
     */
    @After
    public void removeAllProducts() {
        productRepository.deleteAll();
    }

    /**
     * Seed a sample product item for each integration test.
     */
    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webApplicationContext).build();
        productRepository.deleteAll();

        Product product = Product.builder()
                .name("The Big Lebowski (Blu-ray)")
                .id(productId)
                .currentPrice(CurrentPrice.builder()
                        .currencyCode(CurrencyCode.USD)
                        .value(new BigDecimal(199.99))
                        .build())
                .build();

        product = productRepository.save(product);

        assertThat(product).isNotNull();
        assertThat(product.getId()).isNotNull();
    }

    @Test
    public void testNotFound() throws Exception {
        mockMvc.perform(get("/products/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testFindProductWithPriceSaved() throws Exception {

        mockMvc.perform(get("/products/{id}", productId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(13860428)))
                .andExpect(jsonPath("$.name", is("The Big Lebowski (Blu-ray)")))
                .andExpect(jsonPath("$.current_price.value", is(199.99)))
                .andExpect(jsonPath("$.current_price.currency_code", is("USD")));
    }

    @Test
    public void testFindProductWithOutPriceSaved() throws Exception {

        mockMvc.perform(get("/products/{id}", 16696652))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(16696652)))
                .andExpect(jsonPath("$.name", is("Beats Solo 2 Wireless - Black (MHNG2AM/A)")))
                .andExpect(jsonPath("$.current_price").doesNotExist());
    }

    String json(Object object) throws IOException {
       /* MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();*/
        return objectMapper.writeValueAsString(object);
    }

    @Test
    public void testUpdateProductWithPricing() throws Exception {
        mockMvc.perform(get("/products/{id}", 16696652))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(16696652)))
                .andExpect(jsonPath("$.name", is("Beats Solo 2 Wireless - Black (MHNG2AM/A)")))
                .andExpect(jsonPath("$.current_price").doesNotExist());

        Product product = readProduct();
        assertThat(product).isNotNull();

        CurrentPrice currentPrice = CurrentPrice.builder()
                .currencyCode(CurrencyCode.USD)
                .value(new BigDecimal(99.99))
                .build();
        product.setCurrentPrice(currentPrice);

        ProductDto productDto = ProductDto.map(product);
        assertThat(productDto).isNotNull();

        String productDtoString = json(productDto);
        logger.info(productDtoString);

        mockMvc.perform(put("/products/{id}", product.getId())
                .contentType(contentType)
                .content(productDtoString))
                //.andDo(print())
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/products/{id}", 16696652))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(16696652)))
                .andExpect(jsonPath("$.name", is("Beats Solo 2 Wireless - Black (MHNG2AM/A)")))
                .andExpect(jsonPath("$.current_price.value", is(99.99)))
                .andExpect(jsonPath("$.current_price.currency_code", is("USD")));
    }

    @Test
    public void updateProductWithPricingWithNoValue() throws Exception {
        mockMvc.perform(get("/products/{id}", 16696652))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(16696652)))
                .andExpect(jsonPath("$.name", is("Beats Solo 2 Wireless - Black (MHNG2AM/A)")))
                .andExpect(jsonPath("$.current_price").doesNotExist());

        Product product = readProduct();
        assertThat(product).isNotNull();

        CurrentPrice currentPrice = CurrentPrice.builder()
                .currencyCode(CurrencyCode.USD)
                .build();
        product.setCurrentPrice(currentPrice);

       /* product = productRepository.save(product);
        assertThat(product).isNotNull();*/

        ProductDto productDto = ProductDto.map(product);
        assertThat(productDto).isNotNull();

        String productDtoString = json(productDto);
        logger.info(productDtoString);

        mockMvc.perform(put("/products/{id}", product.getId())
                .contentType(contentType)
                .content(productDtoString))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Value of the Price should be greater than 0")));
    }

    @Test
    public void updateProductWithoutPricingShouldFail() throws Exception {
        mockMvc.perform(get("/products/{id}", 16696652))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(16696652)))
                .andExpect(jsonPath("$.name", is("Beats Solo 2 Wireless - Black (MHNG2AM/A)")))
                .andExpect(jsonPath("$.current_price").doesNotExist());

        Product product = readProduct();
        assertThat(product).isNotNull();

        /*CurrentPrice currentPrice = CurrentPrice.builder()
                .currencyCode(CurrencyCode.USD)
                .build();
        product.setCurrentPrice(currentPrice);*/

       // product = productRepository.save(product);
       // assertThat(product).isNotNull();

        ProductDto productDto = ProductDto.map(product);
        assertThat(productDto).isNotNull();

        String productDtoString = json(productDto);
        logger.info(productDtoString);

        mockMvc.perform(put("/products/{id}", product.getId())
                .contentType(contentType)
                .content(productDtoString))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.[0].message", is("Current price is required.")));
    }

    private Product readProduct() throws IOException {
        List<String> fileContent = FileUtil.getResource("data/16696652.json");
        assertThat(fileContent).isNotNull();

        String jsonContent = fileContent.stream().collect(Collectors.joining());
        ProductResponse productResponse = objectMapper.readValue(jsonContent, ProductResponse.class);

        assertThat(productResponse).isNotNull();
        assertThat(productResponse.getProductJson()).isNotNull();
        assertThat(productResponse.getProductJson().getAvailableToPromise()).isNotNull();

        assertThat(productResponse.getProductJson().getProductItem()).isNotNull();
        assertThat(productResponse.getProductJson().getProductItem().getProductDescription()).isNotNull();

        ProductItem productItem = productResponse.getProductJson().getProductItem();
        return Product.builder()
                .id(Long.valueOf(productItem.getTcin()))
                .name(productItem.getProductDescription().getTitle())
                .build();
    }

}
