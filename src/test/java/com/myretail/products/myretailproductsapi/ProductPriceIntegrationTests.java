package com.myretail.products.myretailproductsapi;

import com.myretail.products.myretailproductsapi.domain.CurrencyCode;
import com.myretail.products.myretailproductsapi.domain.CurrentPrice;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.repository.ProductRepository;
import com.myretail.products.myretailproductsapi.service.ProductPriceService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by 6/19/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyRetailProductsApiApplication.class,
        properties = {
                "spring.profiles.active=dev,unit-test"
        })
public class ProductPriceIntegrationTests {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductPriceService productPriceService;

    private static Long productId = 13860428L;

    /**
     * Cleanup the local database for product prices after each test
     */
    @After
    public void removeAllProducts() {
        productRepository.deleteAll();
    }

    /**
     * Seed a sample product item for each integration test.
     */
    @Before
    public void setup() {
        Product product = Product.builder()
                .name("The Big Lebowski (Blu-ray)")
                .id(productId)
                .currentPrice(CurrentPrice.builder()
                        .currencyCode(CurrencyCode.USD)
                        .value(new BigDecimal(199.99))
                        .build())
                .build();

        product = productRepository.save(product);

        assertThat(product).isNotNull();
        assertThat(product.getId()).isNotNull();
    }

    @Test
    public void findNameAndPriceOfProductSuccessfully() throws IOException {
        Product product = productPriceService.findProductWithPrice(productId);
        assertThat(product).isNotNull();
        assertThat(product.getId()).isEqualTo(productId);
        assertThat(product.getName()).isNotNull().isEqualTo("The Big Lebowski (Blu-ray)");

        assertThat(product.getCurrentPrice()).isNotNull();
        BigDecimal price = product.getCurrentPrice().getValue();
        BigDecimal bigDecimalPrice = price.setScale(2, BigDecimal.ROUND_HALF_UP);
        assertThat(bigDecimalPrice).isEqualByComparingTo("199.99");
    }
}
