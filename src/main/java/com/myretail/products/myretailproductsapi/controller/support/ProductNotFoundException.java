package com.myretail.products.myretailproductsapi.controller.support;

/**
 * Created by 6/19/17.
 */
public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException(String message) {
        super(message);
    }
}
