package com.myretail.products.myretailproductsapi.controller.support;

import com.myretail.products.myretailproductsapi.service.impl.NoProductException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.naming.InvalidNameException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by 6/19/17.
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseStatus(HttpStatus.NOT_FOUND)  // 404
    @ExceptionHandler({NoProductException.class,
            ProductNotFoundException.class})
    @ResponseBody
    public ErrorResponse handleProductNotFound(HttpServletRequest req, Exception ex) {
        return ErrorResponse.builder()
                .url(req.getRequestURL().toString())
                .message(ex.getLocalizedMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)  // 400
    @ExceptionHandler({InvalidProductException.class})
    @ResponseBody
    public ErrorResponse handleBadProductUpdate(HttpServletRequest req, Exception ex) {
        return ErrorResponse.builder()
                .url(req.getRequestURL().toString())
                .message(ex.getLocalizedMessage())
                .build();
    }

}
