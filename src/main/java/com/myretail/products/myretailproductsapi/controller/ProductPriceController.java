package com.myretail.products.myretailproductsapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myretail.products.myretailproductsapi.controller.support.ErrorResponse;
import com.myretail.products.myretailproductsapi.controller.support.InvalidProductException;
import com.myretail.products.myretailproductsapi.controller.support.ProductNotFoundException;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.service.ProductPriceService;
import com.myretail.products.myretailproductsapi.service.ProductRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by 6/18/17.
 */
@RestController
@RequestMapping("/products")
public class ProductPriceController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductPriceController.class);
    private final ProductRestService productRestService;
    private final ProductPriceService productPriceService;
    private final ObjectMapper objectMapper;

    public ProductPriceController(ProductRestService productRestService,
                                  ObjectMapper objectMapper,
                                  ProductPriceService productPriceService) {
        this.productRestService = productRestService;
        this.productPriceService = productPriceService;
        this.objectMapper = objectMapper;
    }

    /**
     * Retrieves in sequential order a product from http://redsky.target.com/v2/pdp/tcin/%s?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics
     * then if available populates the pricing information from the local db before returning to the user
     *
     * @param id of the product we are interested in.
     * @return A product with pricing information if available, else 404 if the product does not exist
     * or partial product without pricing information if the product exists but no pricing information.
     */
    @GetMapping({"/{id}"})
    public ProductDto findProductById(@PathVariable("id") Long id) {
        LOGGER.info("Received Request To Process Product Id: {}", id);
        Product product;

        //wrap any IOException to a checked exception such as ProductNotFound
        //which gets translated into a more meaningful response to the caller.
        try {
            product = productPriceService.findProductWithPrice(id);
        } catch (IOException e) {
            throw new ProductNotFoundException("An error occurred trying to process product: "
                    + id + ". Please try again later");
        }
        if (product == null) {
            throw new ProductNotFoundException("Product with Id: " + id + " was not found");
        }
        return ProductDto.map(product);
    }

    /**
     * Receives a request to Update the pricing information of a particular product given
     * the id of the product.
     *
     * @param id         of the product we are attempting to update
     * @param productDto the requestbody of the productDto which we are trying to update.
     *                   this should be a valid object else an exception is thrown.
     * @param errors     Any binding errors seen after the request has been received. Any errors
     *                   will stop the processing of the request.
     * @return A response entity, which may include errors encountered or just a NoContent header
     * after the request has successfully been processed.
     * @throws IOException
     */
    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> updateProductPricing(@PathVariable("id") Long id,
                                                  @Valid @RequestBody ProductDto productDto,
                                                  //@RequestBody String productDtoString,
                                                  Errors errors) throws IOException {
        //LOGGER.info("Received ProductDtoString: {}", productDtoString);
        //ProductDto productDto = objectMapper.readValue(productDtoString, ProductDto.class);
        LOGGER.info("Received request to update product pricing info for product id: {}", id);
        //If error, just return a 400 bad request, along with all the error messages
        if (errors.hasErrors()) {
            LOGGER.debug("There were validation errors present.");
            List<ErrorResponse> errorResponses =
                    errors.getAllErrors()
                            .stream()
                            .map(error -> {
                                LOGGER.debug("Processing Error: {}", error.getDefaultMessage());
                                return ErrorResponse.builder()
                                        .message(error.getDefaultMessage())
                                        .build();
                            })
                            .collect(Collectors.toList());

            errorResponses.forEach(error -> LOGGER.debug("Error Message: {}", error.getMessage()));
            return ResponseEntity.badRequest().body(errorResponses);
        }

        //validate that users add the correct price
        if (productDto.getCurrentPriceDto().getValue() <= 0)
            throw new InvalidProductException("Value of the Price should be greater than 0");

        Product product = productDto.toProduct();
        if (product == null)
            throw new InvalidProductException("Couldn't save the product pricing information");

        //now save the product in the database.
        product = productRestService.savePricingInfo(product);

        if (product == null)
            throw new InvalidProductException("Couldn't save the product pricing information");

        return ResponseEntity.noContent().build();
    }
}
