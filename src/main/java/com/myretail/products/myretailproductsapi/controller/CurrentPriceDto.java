package com.myretail.products.myretailproductsapi.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myretail.products.myretailproductsapi.domain.CurrentPrice;
import com.myretail.products.myretailproductsapi.util.jackson.MoneyConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by 6/18/17.
 */
@Data
@Builder
@AllArgsConstructor
@JsonDeserialize(builder = CurrentPriceDto.CurrentPriceDtoBuilder.class)
public class CurrentPriceDto {
    @JsonSerialize(using = MoneyConverter.class)
    @JsonIgnore
    private Double value;

    @NotNull(message = "currency_code cannot be null")
    @JsonProperty("currency_code")
    private String currencyCode;

    @Tolerate
    public CurrentPriceDto() {
    }

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static final class CurrentPriceDtoBuilder {

    }
}
