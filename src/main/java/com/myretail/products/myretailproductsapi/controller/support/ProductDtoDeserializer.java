package com.myretail.products.myretailproductsapi.controller.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.myretail.products.myretailproductsapi.controller.CurrentPriceDto;
import com.myretail.products.myretailproductsapi.controller.ProductDto;
import com.myretail.products.myretailproductsapi.domain.CurrentPrice;

import java.io.IOException;

/**
 * Used to Manually deserialize the input value which is used as way to update
 * product pricing in the local database.
 * Created by 6/19/17.
 */
public class ProductDtoDeserializer extends StdDeserializer<ProductDto> {
    public ProductDtoDeserializer(Class<?> vc) {
        super(vc);
    }

    public ProductDtoDeserializer() {
        this(null);
    }

    @Override
    public ProductDto deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        ProductDto.ProductDtoBuilder builder = ProductDto.builder();
        Long id = node.get("id").asLong();
        String name = node.get("name").asText();
        builder.id(id).name(name);

        if (node.hasNonNull("current_price")) {
            JsonNode currentPrice = node.get("current_price");
            double value = 0;
            if (currentPrice.hasNonNull("value"))
                value = currentPrice.get("value").asDouble();

            String currencyCode = null;
            if (currentPrice.hasNonNull("currency_code"))
                currencyCode = currentPrice.get("currency_code").asText();
            CurrentPriceDto currentPriceDto = CurrentPriceDto.builder()
                    .value(value)
                    .currencyCode(currencyCode)
                    .build();
            builder.currentPriceDto(currentPriceDto);
        }
        return builder.build();
    }
}
