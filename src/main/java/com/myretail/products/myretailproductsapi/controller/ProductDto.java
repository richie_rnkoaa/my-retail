package com.myretail.products.myretailproductsapi.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.myretail.products.myretailproductsapi.controller.support.ProductDtoDeserializer;
import com.myretail.products.myretailproductsapi.domain.CurrencyCode;
import com.myretail.products.myretailproductsapi.domain.CurrentPrice;
import com.myretail.products.myretailproductsapi.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.data.cassandra.mapping.Column;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by 6/19/17.
 */
@Data
@Builder
@AllArgsConstructor
@JsonDeserialize(using = ProductDtoDeserializer.class)
public class ProductDto {
    @NotNull
    private Long id;

    //Not necessary stored in the pricing table, but it helps
    //to identify the product
    private String name;

    @JsonProperty("current_price")
    @NotNull(message = "Current price is required.")
    private CurrentPriceDto currentPriceDto;

    @Tolerate
    public ProductDto() {
    }

    /**
     * Constructs a Product DTO object from a product. Prevents
     * the leaking of internal domain values which are not ready for public consumption.
     *
     * @param product the non null object from which to construct the dto
     * @return Returns a valid DTO object which can be sent across the network.
     */
    public static ProductDto map(Product product) {
        Objects.requireNonNull(product, "Product cannot be null.");
        ProductDtoBuilder builder = builder();
        builder.id(product.getId()).name(product.getName());

        if (product.getCurrentPrice() != null) {
            CurrentPriceDto.CurrentPriceDtoBuilder currentPriceDtoBuilder = CurrentPriceDto.builder();
            currentPriceDtoBuilder
                    .currencyCode(product.getCurrentPrice().getCurrencyCode().name());

            BigDecimal priceValue = product.getCurrentPrice().getValue();
            if (priceValue != null) {
                priceValue = priceValue.setScale(2, BigDecimal.ROUND_HALF_UP);
                currentPriceDtoBuilder.value(priceValue.doubleValue());
            }
            builder.currentPriceDto(currentPriceDtoBuilder.build());
        }
        return builder.build();
    }

    /**
     * After the validation the request later,
     * all fields are expected to be set before being converted to the internal domain object.
     *
     * @return A Product which is constructed from the dto.
     */
    public Product toProduct() {
        Objects.requireNonNull(this, "Product cannot be null.");
        Product.ProductBuilder builder = Product.builder();
        builder.name(this.name)
                .id(this.id)
                .currentPrice(CurrentPrice.builder()
                        .value(new BigDecimal(this.currentPriceDto.getValue()))
                        .currencyCode(CurrencyCode.valueOf(this.currentPriceDto.getCurrencyCode()))
                        .build());
        return builder.build();
    }

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static final class ProductDtoBuilder {

    }
}
