package com.myretail.products.myretailproductsapi.controller.support;

import lombok.Builder;
import lombok.Value;

/**
 * Created by 6/19/17.
 */
@Value
@Builder
public class ErrorResponse {
    private final String url;
    private final String message;
}