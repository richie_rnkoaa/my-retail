package com.myretail.products.myretailproductsapi.controller.support;

/**
 * Created by 6/19/17.
 */
public class InvalidProductException extends RuntimeException {
    public InvalidProductException(String message) {
        super(message);
    }
}
