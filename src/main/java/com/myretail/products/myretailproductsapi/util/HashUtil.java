package com.myretail.products.myretailproductsapi.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created on 12/13/2016.
 */
public class HashUtil {

  private static final Object lock = new Object();
  private static MessageDigest messageDigest;

  static {
    try {
      messageDigest = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  public synchronized static String hashString(String id) throws NoSuchAlgorithmException {
    messageDigest.update(id.getBytes());
    byte byteData[] = messageDigest.digest();

    //convert the byte to hex format method 1
    StringBuilder sb = new StringBuilder();
    for (byte aByteData : byteData) {
      sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString().substring(0, 10);
  }
}
