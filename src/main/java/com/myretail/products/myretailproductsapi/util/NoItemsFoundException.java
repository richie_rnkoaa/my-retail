package com.myretail.products.myretailproductsapi.util;

/**
 * Created by rnkoaa on 3/12/17.
 */
public class NoItemsFoundException extends RuntimeException {
  public NoItemsFoundException(String message) {
    super(message);
  }
}
