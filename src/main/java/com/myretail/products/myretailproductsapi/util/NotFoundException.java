package com.myretail.products.myretailproductsapi.util;

/**
 * Created by rnkoaa on 3/12/17.
 */
public class NotFoundException extends RuntimeException {
  public NotFoundException(String message) {
    super(message);
  }
}
