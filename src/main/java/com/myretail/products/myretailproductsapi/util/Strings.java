package com.myretail.products.myretailproductsapi.util;

import java.security.NoSuchAlgorithmException;

/**
 * Created by U0165547 on 2/28/2017.
 */
public class Strings {
  public static String removeSpecialChars(String value) {
    //value = value.replaceAll("[^a-zA-Z0-9-\\\\s+]", "");
    value = value.replaceAll("[;,:+!./'?&\\(\\)\\\\]", "");
    return value;
  }


  public static String createVideoUrl(String title, String id) {
    title = Strings.removeSpecialChars(title);
    String endpoint = "";

    try {
      //remove ;,:+!./'?&()

      String hash = HashUtil.hashString(title + id);
      title = title.replace(' ', '-');
      endpoint = hash + "-" + title.toLowerCase();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return endpoint;

  }
}
