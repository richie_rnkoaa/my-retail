package com.myretail.products.myretailproductsapi.util.file;

import com.google.common.collect.Lists;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.util.StringUtils.isEmpty;
import static org.springframework.util.StringUtils.trimWhitespace;

/**
 * Created using Intellij IDE
 * Created by rnkoaa on 8/4/16.
 */
public class FileUtil {

  public static String getMappingContent(String fileLocation) throws IOException {
    return getFileContent(fileLocation);
  }

  public static String getFileContent(String fileLocation) throws IOException {
    Path filePath = Paths.get(fileLocation);
    byte[] file = Files.readAllBytes(filePath);
    return new String(file, Charset.defaultCharset());
  }

  public static List<String> getResource(String location) {
    InputStream in = FileUtil.class.getClassLoader().getResourceAsStream(location);
    List<String> lines = Lists.newArrayList();
    String line;

    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in))) {
      while ((line = bufferedReader.readLine()) != null) {
        if (!isEmpty(trimWhitespace(line)) && !line.startsWith("--"))
          lines.add(line);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return lines;
  }
}
