package com.myretail.products.myretailproductsapi.context;

import com.myretail.products.myretailproductsapi.domain.CurrentPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.config.java.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

import org.springframework.data.cassandra.convert.CustomConversions;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 6/17/17.
 */
@Configuration
@EnableCassandraRepositories
public class CassandraConfig extends AbstractCassandraConfiguration {

    @Autowired
    private CassandraPropertiesConfig cassandraPropertiesConfig;

    @Override
    protected String getKeyspaceName() {
        return cassandraPropertiesConfig.getKeyspace();
    }

    @Override
    public CustomConversions customConversions() {

        List<Converter<?, ?>> converters = new ArrayList<>();

        return new CustomConversions(converters);
    }

    /*
   * Provide a contact point to the configuration.
   */
    public String getContactPoints() {
        return cassandraPropertiesConfig.getContactPoints();
    }
/*
    static class CurrencyPriceReadConverter implements Converter<Object, Object> {

        @Override
        public Object convert(Object source) {
            return null;
        }
    }

    static class CurrencyPriceWriteConverter implements Converter<CurrentPrice, Object> {

        @Override
        public Object convert(Object source) {
            return null;
        }
    }*/
}
