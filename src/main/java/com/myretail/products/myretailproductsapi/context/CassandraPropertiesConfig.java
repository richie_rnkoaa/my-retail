package com.myretail.products.myretailproductsapi.context;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by 6/17/17.
 */
@Configuration
@ConfigurationProperties(prefix = "cassandra")
@Data
@NoArgsConstructor
@AllArgsConstructor
@PropertySource("classpath:cassandra-dev.properties")
public class CassandraPropertiesConfig {
    private String keyspace;
    private int port;
    private String contactPoints;
}
