package com.myretail.products.myretailproductsapi.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myretail.products.myretailproductsapi.repository.ProductRepository;
import com.myretail.products.myretailproductsapi.service.ProductRestService;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.rest.ProductItem;
import com.myretail.products.myretailproductsapi.rest.ProductJson;
import com.myretail.products.myretailproductsapi.rest.ProductResponse;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by 6/17/17.
 */
@Service("productRestService")
public class ProductRestServiceImpl implements ProductRestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRestServiceImpl.class);

    private final OkHttpClient okHttpClient;
    private final ObjectMapper objectMapper;
    private final String productUrlBase;
    private final ProductRepository productRepository;

    public ProductRestServiceImpl(@Value("${product.url.base}") String productUrlBase,
                                  OkHttpClient okHttpClient,
                                  ProductRepository productRepository,
                                  ObjectMapper objectMapper) {
        this.okHttpClient = okHttpClient;
        this.objectMapper = objectMapper;
        this.productUrlBase = productUrlBase;
        this.productRepository = productRepository;
    }

    @Override
    public Product findProduct(Long productId) throws IOException {
        //the base url format only takes the product id as a string to construct the proper
        //item url.

        final String productUrl = String.format(productUrlBase, String.valueOf(productId));

        return makeGetRequest(productId, productUrl);
    }

    @Override
    public Product savePricingInfo(Product product) {
        Objects.requireNonNull(product, "Product cannot be a null object");
        LOGGER.info("Saving pricing info for product: {}", product.getName());
        return productRepository.save(product);
    }

    /**
     * Private method to reactively make an http request. This ensures that
     * the request does not block other requests in the pipeline.
     *
     * @param url
     * @return
     */
    private Product makeGetRequest(final Long productId, String url) throws IOException {
        Product product = null;
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = okHttpClient.newCall(request).execute();
        if (!response.isSuccessful()) {
            return null;
        }

        String responseBody = response.body().string();
        response.body().close();
        ProductResponse productResponse = objectMapper.readValue(responseBody, ProductResponse.class);
        //step through the object to find the product item that we are interested in potentially
        //avoiding a nullpointer exception in the object structure.
        if (null != productResponse) {
            ProductJson productJson = productResponse.getProductJson();
            if (productJson != null) {
                ProductItem productItem = productJson.getProductItem();
                if (productItem != null) {
                    product = ProductItem.map(productItem);
                    //return whatever product was mapped successfully.
                }
            }
        }
        return product;
    }
}
