package com.myretail.products.myretailproductsapi.service.impl;

import com.myretail.products.myretailproductsapi.repository.ProductRepository;
import com.myretail.products.myretailproductsapi.service.ProductRestService;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.service.ProductPriceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by 6/19/17.
 */
@Service
public class ProductPriceServiceImpl implements ProductPriceService {
    private static final Logger logger = LoggerFactory.getLogger(ProductPriceServiceImpl.class);

    private final ProductRepository productRepository;
    private final ProductRestService productRestService;

    public ProductPriceServiceImpl(ProductRepository productRepository,
                                   ProductRestService productRestService) {
        this.productRepository = productRepository;
        this.productRestService = productRestService;
    }

    @Override
    public Product findUpdatePrice(Product product) {
        Objects.requireNonNull(product, "The product object cannot be null");
        if (product.getId() > 0L) {
            Product productOptional = productRepository.findOne(product.getId());
            if (productOptional != null)
                product.setCurrentPrice(productOptional.getCurrentPrice());
            return product;
        }
        return product;
    }

    @Override
    public Product findProductWithPrice(Long productId) throws IOException {
        Product product = null;
        if (productId > 0L) {
            product = productRestService.findProduct(productId);
            if (product != null) {
                logger.info("Received Product With Name: " + product.getName());

                //now update the price of the product from the local repository
                product = findUpdatePrice(product);
            }
        }
        return product;
    }
}
