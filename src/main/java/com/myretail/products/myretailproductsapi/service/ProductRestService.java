package com.myretail.products.myretailproductsapi.service;

import com.myretail.products.myretailproductsapi.domain.Product;

import java.io.IOException;

/**
 * Created by 6/17/17.
 */
public interface ProductRestService {

    /**
     * @param productId
     * @return
     * @throws IOException
     */
    Product findProduct(Long productId) throws IOException;

    /**
     * @param product
     * @return
     */
    Product savePricingInfo(Product product);
}
