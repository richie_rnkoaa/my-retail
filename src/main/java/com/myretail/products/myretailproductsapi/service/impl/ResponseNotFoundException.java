package com.myretail.products.myretailproductsapi.service.impl;

/**
 * Created by 6/19/17.
 */
public class ResponseNotFoundException extends RuntimeException {
    public ResponseNotFoundException(String message) {
        super(message);
    }
}
