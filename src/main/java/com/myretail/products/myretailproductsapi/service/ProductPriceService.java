package com.myretail.products.myretailproductsapi.service;

import com.myretail.products.myretailproductsapi.domain.Product;

import java.io.IOException;

/**
 * Created by 6/19/17.
 */
public interface ProductPriceService {

    /**
     * We may have the price of the product in a different storage engine, so
     * Accept a product then use the id of the product to update the current
     * price of the same object
     *
     * @param product The product whose price needs to be updated.
     * @return The same product that was passed in after price was updated.
     */
    Product findUpdatePrice(Product product);

    /**
     * @param productId
     * @return
     */
    Product findProductWithPrice(Long productId) throws IOException;
}
