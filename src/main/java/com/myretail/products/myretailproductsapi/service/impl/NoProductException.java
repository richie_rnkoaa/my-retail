package com.myretail.products.myretailproductsapi.service.impl;

/**
 * Created by 6/18/17.
 */
public class NoProductException extends RuntimeException {
    public NoProductException(String message) {
        super(message);
    }
}
