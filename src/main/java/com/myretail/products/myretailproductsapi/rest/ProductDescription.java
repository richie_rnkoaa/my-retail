package com.myretail.products.myretailproductsapi.rest;

import lombok.Data;

/**
 * Created by 6/17/17.
 */
@Data
public class ProductDescription {
    private String title;
}
