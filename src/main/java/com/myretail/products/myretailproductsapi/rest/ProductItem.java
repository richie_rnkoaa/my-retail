package com.myretail.products.myretailproductsapi.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.myretail.products.myretailproductsapi.domain.Product;
import lombok.Data;

import java.util.Objects;

/**
 * Created by 6/17/17.
 */
@Data
public class ProductItem {
    private String tcin;
    @JsonProperty("product_description")
    private ProductDescription productDescription;

    public static Product map(ProductItem productItem) {
        Objects.requireNonNull(productItem, "The Item cannot be null");
        Product.ProductBuilder productBuilder = Product.builder();
        if (productItem.getTcin() != null && !productItem.getTcin().isEmpty()) {
            productBuilder.id(safeConvert(productItem.getTcin()));
        }

        if (productItem.getProductDescription() != null) {
            productBuilder.name(productItem.getProductDescription().getTitle());
        }
        return productBuilder.build();
    }

    private static Long safeConvert(String tcin) {
        long value = 0;
        try {
            value = Long.valueOf(tcin);
        } catch (NumberFormatException ex) {
            //not handling for now;
            System.out.println(ex.getLocalizedMessage());
        }
        return value;
    }
}
