package com.myretail.products.myretailproductsapi.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by 6/17/17.
 */
@Data
public class ProductJson {

    @JsonProperty("available_to_promise_network")
    private AvailableToPromise availableToPromise;

    @JsonProperty("item")
    private ProductItem productItem;
}