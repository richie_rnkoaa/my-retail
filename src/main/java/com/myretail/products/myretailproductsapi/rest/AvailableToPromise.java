package com.myretail.products.myretailproductsapi.rest;

/**
 * Created by 6/17/17.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AvailableToPromise {
    @JsonProperty("product_id")
    private Long productId;
}
