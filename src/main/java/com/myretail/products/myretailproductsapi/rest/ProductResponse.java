package com.myretail.products.myretailproductsapi.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by 6/17/17.
 */
@Data
public class ProductResponse {
    @JsonProperty("product")
    private ProductJson productJson;

}
