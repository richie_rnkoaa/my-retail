package com.myretail.products.myretailproductsapi.domain;

//import com.datastax.driver.mapping.annotations.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

/**
 * Created by 6/17/17.
 */
@Data
@Builder
@AllArgsConstructor
@Table("product_prices")
public class Product {

    @PrimaryKey("product_id")
    private Long id;

    //Not necessary stored in the pricing table, but it helps
    //to identify the product
    @Column("product_name")
    private String name;

    @JsonProperty("current_price")
    @Column("price")
    private CurrentPrice currentPrice;

    @Tolerate
    public Product() {
    }
}
