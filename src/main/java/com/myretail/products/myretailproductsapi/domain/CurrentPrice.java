package com.myretail.products.myretailproductsapi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myretail.products.myretailproductsapi.util.jackson.MoneyConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.Table;
import org.springframework.data.cassandra.mapping.UserDefinedType;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by 6/17/17.
 */
@Data
@Builder
@UserDefinedType("current_price")
@AllArgsConstructor
public class CurrentPrice {
    @JsonSerialize(using = MoneyConverter.class)
    private BigDecimal value;

    @Column("currency_code")
    @JsonProperty("currency_code")
    private CurrencyCode currencyCode;

    @Tolerate
    public CurrentPrice() {
    }

}
