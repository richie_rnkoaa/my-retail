package com.myretail.products.myretailproductsapi;

import com.myretail.products.myretailproductsapi.domain.CurrencyCode;
import com.myretail.products.myretailproductsapi.domain.CurrentPrice;
import com.myretail.products.myretailproductsapi.domain.Product;
import com.myretail.products.myretailproductsapi.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

@SpringBootApplication
public class MyRetailProductsApiApplication {
    @Bean
    CommandLineRunner commandLineRunner(ProductRepository productRepository) {
        Long productId = 13860428L;
        return (args -> {
            Product product = Product.builder()
                    .name("The Big Lebowski (Blu-ray)")
                    .id(productId)
                    .currentPrice(CurrentPrice.builder()
                            .currencyCode(CurrencyCode.USD)
                            .value(new BigDecimal(199.99))
                            .build())
                    .build();

            product = productRepository.save(product);
        });
    }

    public static void main(String[] args) {
        SpringApplication.run(MyRetailProductsApiApplication.class, args);
    }
}
