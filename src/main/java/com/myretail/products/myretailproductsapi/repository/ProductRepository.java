package com.myretail.products.myretailproductsapi.repository;

import com.myretail.products.myretailproductsapi.domain.Product;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Created by 6/17/17.
 */
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query("SELECT * from product_prices where product_id = ?0")
    Optional<Product> findById(Long id);
}
