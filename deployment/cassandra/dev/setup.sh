#!/usr/bin/env bash

docker run -it --link cassandra_dev --rm cassandra:3 cqlsh cassandra_dev

docker run -it --net=myretail_default --link cassandra_dev --rm cassandra:3 cqlsh cassandra_dev

# when joining a network
# docker run -it --net=local_kuiper_network --link cassandra_dev --rm cassandra:3.9 cqlsh cassandra_dev

CREATE KEYSPACE IF NOT EXISTS my_retail_products
WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};

use my_retail_products;

CREATE TYPE IF NOT EXISTS my_retail_products.current_price (
  value decimal,
  currency_code text
);

CREATE TABLE IF NOT EXISTS my_retail_products.product_prices (
    product_id BIGINT,
    product_name TEXT,
    price current_price,
    PRIMARY KEY(product_id)
);