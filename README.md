## MyRetail REST Webservices

*MyRetail* is a RESTful api service which provides the following services to various client applications
1. The ability to lookup pricing information of a product given the id of the product.
2. The ability to update the pricing information of the product in a different database. 

## Building and Running My Retail Services ##
##### Prerequisites

* git
* Java 1.8
* Gradle 3.4 or higher
* Unix environment (bash script)
* Docker
* Docker-compose
* Cassandra NoSQL Storage

Ensure that the Java version on the classpath is version 1.8 b121 or higher
Checkout the code from the git repo located at [this bit bucket location](https://bitbucket.org/richie_rnkoaa/my-retail)

1. Ensure that Docker `1.12` or higher is installed.
2. Install the latest docker-compose.
3. go into the `deployment/cassandra/dev` folder of the root directly
4. Start the cassandra docker file by running
    
    ```sh docker-compose up -d```

5. Once the container is up, lets update the application.
    * Run the following command to login to the cassandra docker container
    
    ```sh 
    docker run -it --net=myretail_default --link cassandra_dev --rm cassandra:3 cqlsh cassandra_dev
    ```
    
    * Once in the cql prompt of the docker container, create the keyspace and schema needed by the application with the following commands
    
    ```sh
    CREATE KEYSPACE IF NOT EXISTS my_retail_products
    WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};
    
    use my_retail_products;
    
    CREATE TYPE IF NOT EXISTS my_retail_products.current_price (
      value decimal,
      currency_code text
    );
    
    CREATE TABLE IF NOT EXISTS my_retail_products.product_prices (
        product_id BIGINT,
        product_name TEXT,
        price current_price,
        PRIMARY KEY(product_id)
    );
    ```
    
    * Exit the container.
    
6. Build the application from the root container using the following gradle command

    ```sh ./gradlew clean assemble```
  
7. Ensure the tests pass by running all the available unit tests
    
    ```sh ./gradlew test```
 
8. Once all the tests pass, run the application using either gradle or as a java jar application
    * using gradle
    ```sh
    ./gradle bootRun
    ```
    
    * As a java application
    ```sh
    java -jar build/libs/myretail-products-api-0.0.1-SNAPSHOT.jar
    
    ```
    
9. Access the health of the application at `http://localhost:8080/health`. A successful
deployment will include 
```{ "status": "UP" } ``` in the response which indicates that the application is up and running.

## Using the Application ##

1. #### Getting Product Pricing Details
   ```sh
       Method: Get
       Headers: Content-Type: application/json
       Url: http://localhost:8080/products/{id}
       
       # Sample Response with pricing information
        Status: 200 (successful)
       {
           "id":16696652,
           "name":"Beats Solo 2 Wireless - Black (MHNG2AM/A)",
           "current_price":{
               "value":99.99,
               "currency_code":"USD"
           }
       }
       
      # Sample Response without pricing information
      Status: 200 (successful)
      {
          "id":16696652,
          "name":"Beats Solo 2 Wireless - Black (MHNG2AM/A)"
      }
   ``` 
   
2. #### Updating a pricing information
   ```sh
         Method: Put
         Headers: Content-Type: application/json
         Url: http://localhost:8080/products/{id}
         
         # Sample Request Body
         {
             "id":16696652,
             "name":"Beats Solo 2 Wireless - Black (MHNG2AM/A)",
             "current_price":{
                 "value":99.99,
                 "currency_code":"USD"
             }
         }
         
         #sample Response:
         Status: 204 (successful)
                 4XX (failure with message body)
     ```   

